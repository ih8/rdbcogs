from .pigtools import PigTools


async def setup(bot):
    pigtools = PigTools(bot)
    await pigtools.create_cache()
    bot.add_cog(pigtools)
