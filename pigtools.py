import discord
from discord.ext import commands
from .utils.dataIO import dataIO
from .utils import checks
from __main__ import send_cmd_help, settings
# from datetime import datetime
from collections import deque, defaultdict, OrderedDict
from cogs.utils.chat_formatting import escape_mass_mentions, box, pagify
import os
import asyncio

PGROUP_LOCATION = "data/ffp/pgroup.json"
PFILTER_LOCATION = "data/ffp/pfilter.json"

class PigTools:
	"""My custom cog that does stuff!"""

	# class Group:
	# 	def __init__(self, roles, channels):
	# 		self.roles = roles
	# 		self.channels = channels

	def __init__(self, bot):
		self.bot = bot

		self.pgroup = dataIO.load_json(PGROUP_LOCATION)
		self.pfilter = dataIO.load_json(PFILTER_LOCATION)

#  Group  ===============

	@commands.group(name="pgroup", pass_context=True, no_pm=True)
	@checks.mod_or_permissions(manage_messages=True)
	async def _pgroup(self, ctx):
		"""Word filter command.
		For more details, see the add command"""
		if ctx.invoked_subcommand is None:
			await send_cmd_help(ctx)
			# server = ctx.message.server
			# author = ctx.message.author

	@_pgroup.command(name="list", pass_context=True)
	async def pgroup_list(self, ctx):
		"""Prints all groups
		Examples:
		!pgroup list"""
		server = ctx.message.server

		if server.id not in self.pgroup.keys():
			self.pgroup[server.id] = {}
		if server.id not in self.pgroup or len(self.pgroup[server.id]) == 0:
			await self.bot.say("No groups exists")
		else:
			keys = ""
			for key, value in self.pgroup[server.id].items() :
				keys += key + ", "
			keys = keys[:-2]
			await self.bot.say(keys)


	@_pgroup.command(name="addgroup", pass_context=True)
	async def pgroup_addgroup(self, ctx, *names: str):
		"""Adds a group
		Examples:
		!pgroup addgroup mygroup
		!pgroup addgroup mygroup1 mygroup2"""
		# names = names.lower()
		server = ctx.message.server

		result = ""
		for name in names:
			name = name.lower()
			if server.id not in self.pgroup.keys():
				self.pgroup[server.id] = {}
			if name not in self.pgroup[server.id] and name != "":
				self.pgroup[server.id][name] = {"roles":[],"channels":[]}

				dataIO.save_json(PGROUP_LOCATION, self.pgroup)
				result += "Group " + name + " added\n"
			else:
				result += "Group " + name + " already exists\n"

		result = result[:-1]
		await self.bot.say(result)


	@_pgroup.command(name="addroles", pass_context=True)
	async def pgroup_addroles(self, ctx, groupName: str, *roles: str):
		"""Adds a group
		Examples:
		!pgroup addroles mygroup admin mod special"""
		name = groupName.lower()
		server = ctx.message.server

		if server.id not in self.pgroup or (name not in self.pgroup[server.id] and name != ""):
			await self.bot.say("Group " + name + " does not exist")
			return

		successString = ""
		failureString = ""
		for role in roles:
			if role.lower() not in self.pgroup[server.id][name]["roles"]:
				if role not in [r.name.lower() for r in ctx.message.server.roles]:
					failureString += role + ", "
				else:
					self.pgroup[server.id][name]["roles"].append(role.lower())
					successString += role + ", "
			else:
				failureString += role + ", "

		successString = successString[:-2]
		failureString = failureString[:-2]

		if successString:
			await self.bot.say("Roles successfully added: " + successString)
		if failureString:
			await self.bot.say("Roles failed to add: " + failureString)

		# self.pgroup[server.id][name] = {"roles":[],"channels":[]}
		dataIO.save_json(PGROUP_LOCATION, self.pgroup)

	@_pgroup.command(name="addchannels", pass_context=True)
	async def pgroup_addchannels(self, ctx, groupName: str, *channels: str):
		"""Adds a group
		Examples:
		!pgroup addchannels general mod_chat super_secret"""
		name = groupName.lower()
		server = ctx.message.server

		if server.id not in self.pgroup or (name not in self.pgroup[server.id] and name != ""):
			await self.bot.say("Group " + name + " does not exist")
			return

		successString = ""
		failureString = ""
		for channel in channels:
			if channel.lower() not in self.pgroup[server.id][name]["channels"]:
				self.pgroup[server.id][name]["channels"].append(channel.lower())
				successString += channel + ", "
			else:
				failureString += channel + ", "

		successString = successString[:-2]
		failureString = failureString[:-2]

		if successString:
			await self.bot.say("Channels successfully added: " + successString)
		if failureString:
			await self.bot.say("Channels failed to add: " + failureString)

		# self.pgroup[server.id][name] = {"roles":[],"channels":[]}
		dataIO.save_json(PGROUP_LOCATION, self.pgroup)


#  Filter  ===============

	@commands.group(name="pfilter", pass_context=True, no_pm=True)
	@checks.mod_or_permissions(manage_messages=True)
	async def _pfilter(self, ctx):
		"""Word filter command.
		For more details, see the add command"""
		if ctx.invoked_subcommand is None:
			await send_cmd_help(ctx)
			server = ctx.message.server
			author = ctx.message.author
			# if server.id in self.pfilter:
			# 	if self.pfilter[server.id]:
			# 		words = ", ".join(self.pfilter[server.id])
			# 		words = "Filtered in this server:\n\n" + words
			# 		try:
			# 			for page in pagify(words, delims=[" ", "\n"], shorten_by=8):
			# 				await self.bot.send_message(author, page)
			# 		except discord.Forbidden:
			# 			await self.bot.say("I can't send direct messages to you.")


	@_pfilter.command(name="add", pass_context=True)
	async def pfilter_add(self, ctx, group, *words: str):
		"""Adds words to the filter
		Use double quotes to add sentences
		Examples:
		filter add all word1 word2 word3
		filter add all \"This is a sentence\"
		filter add this word1
		filter add group1 word1"""
		if words == ():
			await send_cmd_help(ctx)
			return
		server = ctx.message.server
		added = 0
		if server.id not in self.pfilter.keys():
			self.pfilter[server.id] = []
		for w in words:
			if w.lower() not in self.pfilter[server.id] and w != "":
				self.pfilter[server.id].append(w.lower())
				added += 1
		if added:
			dataIO.save_json(PFILTER_LOCATION, self.pfilter)
			await self.bot.say("Words added to filter.")
		else:
			await self.bot.say("Words already in the filter.")

	@_pfilter.command(name="remove", pass_context=True)
	async def pfilter_remove(self, ctx, group, *words: str):
		"""Remove words from the filter
		Use double quotes to remove sentences
		Examples:
		filter remove word1 word2 word3
		filter remove \"This is a sentence\""""
		if words == ():
			await send_cmd_help(ctx)
			return
		server = ctx.message.server
		removed = 0
		if server.id not in self.pfilter.keys():
			await self.bot.say("There are no filtered words in this server.")
			return
		for w in words:
			if w.lower() in self.pfilter[server.id]:
				self.pfilter[server.id].remove(w.lower())
				removed += 1
		if removed:
			dataIO.save_json(PFILTER_LOCATION, self.pfilter)
			await self.bot.say("Words removed from filter.")
		else:
			await self.bot.say("Those words weren't in the filter.")

	@_pfilter.command(name="list", pass_context=True)
	async def pfilter_list(self, ctx):
		"""Lists the words currently in the filter"""
		server = ctx.message.server
		author = ctx.message.author
		if server.id in self.pfilter:
			if self.pfilter[server.id]:
				words = ", ".join(self.pfilter[server.id])
				words = "Filtered in this server:\n\n" + words
				for page in pagify(words, delims=[" ", "\n"], shorten_by=8):
					await self.bot.say(page)

	@_pfilter.command(name="list_dm", pass_context=True)
	async def pfilter_list_dm(self, ctx):
		"""DMs the user a list of the words currently in the filter"""
		server = ctx.message.server
		author = ctx.message.author
		if server.id in self.pfilter:
			if self.pfilter[server.id]:
				words = ", ".join(self.pfilter[server.id])
				words = "Filtered in this server:\n\n" + words
				try:
					for page in pagify(words, delims=[" ", "\n"], shorten_by=8):
						await self.bot.send_message(author, page)
				except discord.Forbidden:
					await self.bot.say("I can't send direct messages to you.")

	async def check_filter(self, message):
		server = message.server
		if server.id in self.pfilter.keys():
			for w in self.pfilter[server.id]:
				if w in message.content.lower():
					try:
						await self.bot.delete_message(message)
						logger.info("Message deleted in server {}."
									"Filtered: {}"
									"".format(server.id, w))
						return True
					except:
						pass
		return False

	async def on_message(self, message):
		# await self.bot.delete_message(message)
		author = message.author
		if message.server is None or self.bot.user == author:
			return

		valid_user = isinstance(author, discord.Member) and not author.bot

		#  Bots and mods or superior are ignored from the filter
		if not valid_user or self.is_mod_or_superior(message):
			return

		deleted = await self.check_filter(message)
		if not deleted:
			deleted = await self.check_duplicates(message)
		if not deleted:
			deleted = await self.check_mention_spam(message)

	async def on_message_edit(self, _, message):
		author = message.author
		if message.server is None or self.bot.user == author:
			return

		valid_user = isinstance(author, discord.Member) and not author.bot

		if not valid_user or self.is_mod_or_superior(message):
			return

		await self.check_filter(message)

	def is_mod_or_superior(self, obj):
		if isinstance(obj, discord.Message):
			user = obj.author
		elif isinstance(obj, discord.Member):
			user = obj
		elif isinstance(obj, discord.Role):
			pass
		else:
			raise TypeError('Only messages, members or roles may be passed')

		server = obj.server
		admin_role = settings.get_server_admin(server)
		mod_role = settings.get_server_mod(server)

		if isinstance(obj, discord.Role):
			return obj.name in [admin_role, mod_role]

		if user.id == settings.owner:
			return True
		elif discord.utils.get(user.roles, name=admin_role):
			return True
		elif discord.utils.get(user.roles, name=mod_role):
			return True
		else:
			return False

	@commands.command()
	async def pigtest(self):
		"""This does stuff!"""

		#Your code will go here
		await self.bot.say("I can do stuff!")

## End FFP --------------------------------------------------------

def check_folders():
	folders = ("data", "data/ffp/")
	for folder in folders:
		if not os.path.exists(folder):
			print("Creating " + folder + " folder...")
			os.makedirs(folder)

def check_files():
	ignore_list = {"SERVERS": [], "CHANNELS": []}

	files = {
		"pgroup.json"         : {},
		"pfilter.json"         : {}
	}

	for filename, value in files.items():
		if not os.path.isfile("data/ffp/{}".format(filename)):
			print("Creating empty {}".format(filename))
			dataIO.save_json("data/ffp/{}".format(filename), value)

# async def create_cache(self):
# 	"""Helper function for generating the channel cache."""
# 	config_guilds = await self.config.all_guilds()
# 	prep_cache = {}
# 	for guild, data_guild in config_guilds.items():
# 		for guild, data in data_guild.items():
# 			for channel, data in data.items():  # DEEPER!!
# 				prep_cache[channel] = data
# 	self.channel_cache = prep_cache
# 	return True

# \todo move this function to __init__.py, needs some changes. see: https://git.kowlin.xyz/Kowlin/FortV3/blob/master/trc/__init__.py
def setup(bot):
	check_folders()
	check_files()
	ffp = PigTools(bot)
	bot.add_listener(ffp.on_message, "on_message")
	# await ffp.create_cache()
	bot.add_cog(ffp)
